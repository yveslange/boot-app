module.exports = (name) => {
  return require('pino')({
    prettyPrint: true,
    name: name,
    base: null,
  })
};
