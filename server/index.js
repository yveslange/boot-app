const log = require('./log')('INDEX');
const DB = require('./db');
const express = require('express');
const app = express();

const CONFIG = {
  port: 8081,
  public_path: "../client/build"
};

DB.connect( (db) => {
  app.use(express.static(__dirname + '/' + CONFIG.public_path));

  app.listen(CONFIG.port, function () {
    log.info(`Application serving ${CONFIG.public_path} files on port ${CONFIG.port}!`);
  });

});

