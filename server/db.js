module.exports = exp = {};

const log = require('./log')("DB")
const MongoClient = require('mongodb').MongoClient;
const DB_NAME = 'db-fast-www';
var url = 'mongodb://localhost:27017/';

var DB = null;

/** Connects to the DB and continue */
exp.connect = (next) => {
  MongoClient.connect(url, (err, db) => {
    if(err) throw err;
    DB = db.db(DB_NAME);
    log.info(`Database \`${DB_NAME}\` created`);
    initialize(DB);
    next(DB);
  });
};

/**
 * Initialize the collections of the database.
 *
 *   - Create the admin user
 */
var initialize = (db) => {
  db.createCollection('users', (err, collection) => {
    if(err) throw err;
    log.info('Collection `users` successfully created!');

    // Create the default 'admin' user if he doesn't exists
    adminUser = {user: 'admin', password: '1234admin'};
    // collection.deleteOne({user: adminUser.user}) // For debugging
    collection.findOne({user: adminUser.user}, (err, result) => {
      if(err) throw err;
      if(!result) {
        collection.insert(adminUser);
        log.warn("Admin user created:", adminUser);
      }
    });

  });
}

/** Close the DB connection */
exp.close = () => {
  if(DB) DB.close();
};



